package com.springapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/base")
public class BaseController {
	@RequestMapping(method = RequestMethod.GET)
	public String print(ModelMap model) {
		model.addAttribute("message", "It's a Base page!");
		return "base";
	}
}